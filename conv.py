import numpy as np
import argparse
import cv2 as  cv

#Perform just one iteration of a convolution given two matrix
def dot_matrix_multiplication(x,y,n):
    w = np.resize(x,n*n)
    v = np.resize(y,n*n)
    return np.dot(w,v)
def extract_submatrix(x,y,padding,matrix):
    return matrix[x-padding:x+padding+1,y-padding:y+padding+1]

def gaussianFilter(n,m,sigma):
    x = np.linspace(-(m-1)/2,(m-1)/2,m)
    y = np.linspace(-(n-1)/2,(n-1)/2,n)
    xv, yv = np.meshgrid(x,y)

    hg = np.exp(-(xv**2+yv**2)/(2*sigma**2))
    h = hg/(2*np.pi*(sigma**2))

    return h
def convolution(kernel, matrix):
    m,n = matrix.shape[:2]
    u,v = kernel.shape[:2]
    pad =int((u-1)/2)
    submatrix = []
    convolved = np.zeros((m,n))
    aux = 0
    #--convolution find center of the kernel
    xKcenter = int(u/2)
    yKcenter = int(v/2)

    for i in range(0,m):
        for j in range(0,n):
            for x in range(0,u):
                xx = u - 1 - x
                for y in range(0,v):
                    yy = v - 1 - y

                    ii = i + (xKcenter-xx)
                    jj = j + (yKcenter-yy)

                    if ii >= 0 and ii < m and jj >= 0 and jj < n :
                        convolved [i,j] += matrix[ii,jj] * kernel[xx,yy]

    return convolved



def prueba():
    a = np.random.rand(6,6)*10
    a = a.round()
    print(a)
    d = extract_submatrix(1,2,1,a)
    b = np.random.rand(3,3)*10
    b = b.round()
    print(b)
    print(d)
    print(dot_matrix_multiplication(d,b,3))

#
# img = cv.imread("images/texture01.tif",cv.IMREAD_UNCHANGED)
# print(img.shape[:2])
# cv.imshow("image",img)
# cv.waitKey(0)
# cv.destroyAllWindows()
