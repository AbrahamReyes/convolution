import numpy as np
import sys
import cv2
import conv as cnv

def createPadding(matrix,p):
	padding = np.zeros((p,matrix.shape[1]))
	matrix = np.concatenate((matrix,padding),axis=0)
	matrix = np.concatenate((padding,matrix),axis=0)
	padding = np.zeros((p,matrix.shape[0]))
	matrix = np.concatenate((matrix,padding.T),axis=1)
	matrix = np.concatenate((padding.T,matrix),axis=1)

	return matrix

def vectorized_convolution(f,h):
	"""
	This function implements discrete convolution with numpy
	for image processing.
	f - is the input image
	h - is kernel or filter
	"""

	# Image size
	(N,M) = f.shape
	(hn,hm) = h.shape


	# Padding that we're going to use computed with the kernel size
	p = int((h.shape[1] -1)/2)

	# Add padding to the image
	f = createPadding(f,p)

	# Initializing output
	g = np.zeros((N,M))

	# Computing entries of g
	for i in range(p,p+N):
		for j in range(p,p+M):

			""" This part should compute convolution
				extracting a submatrix from the input
				and convolving with the kernel
			""" 
			# Computing the indices we are going to use
			x0 = i - p
			xf = i + p + 1
			y0 = j - p 
			yf = j + p +1
			#print(x0, xf, y0, yf)

			# certainly it would be problems in the boundaries of the image

			# We create the submatrices to convolve 
			subf = f[x0:xf,y0:yf]


			# Finally we compute the entry of the ouput image
			g[i-p,j-p] = np.sum(np.multiply(subf,np.rot90(np.rot90(h))))

	return g/255
 

#input = np.array([[1,2,3],[4,5,6],[7,8,9]])                    
#kernel = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])                
#kernel = np.array([[1,2,1],[2,4,2],[1,2,1]])
kernel = cnv.gaussianFilter(5,5,2)                
#g = vectorized_convolution(input,kernel)  
#print(g)


img = cv2.imread("images/texture01.tif", cv2.IMREAD_UNCHANGED)
cv2.imshow("Texture", img)

blurredImg = vectorized_convolution(img,kernel)
#print(blurredImg)
#print(img)
cv2.imshow("BlurredTexture", blurredImg)
cv2.waitKey(0)
cv2.destroyAllWindows()






	